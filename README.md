# scandicraft_server

Serveur et plugins ScandiCraft

## Setup
1. workspace eclipse: ...\scandicraft_server\workspace
2. import existing maven project (only spigot)

## Build & Test
1. run: mvn clean install (sur spigot-parent)
2. launch: ...\scandicraft_server\spigot\Spigot\Spigot-Server\scandicraft_server\start_server.cmd