package net.minecraft.server;

public interface ArmorMaterial {
  int a(EnumItemSlot paramEnumItemSlot);
  
  int b(EnumItemSlot paramEnumItemSlot);
  
  int a();
  
  SoundEffect b();
  
  RecipeItemStack c();
  
  float e();
}
