package net.minecraft.server;

import java.util.function.Supplier;

public enum EnumArmorMaterial implements ArmorMaterial {
  LEATHER("leather", 5, new int[] { 1, 2, 3, 1 }, 15, SoundEffects.ITEM_ARMOR_EQUIP_LEATHER, 0.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.LEATHER })),
  CHAIN("chainmail", 15, new int[] { 1, 4, 5, 2 }, 12, SoundEffects.ITEM_ARMOR_EQUIP_CHAIN, 0.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.IRON_INGOT })),
  IRON("iron", 15, new int[] { 2, 5, 6, 2 }, 9, SoundEffects.ITEM_ARMOR_EQUIP_IRON, 0.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.IRON_INGOT })),
  GOLD("gold", 7, new int[] { 1, 3, 5, 2 }, 25, SoundEffects.ITEM_ARMOR_EQUIP_GOLD, 0.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.GOLD_INGOT })),
  DIAMOND("diamond", 33, new int[] { 3, 6, 8, 3 }, 10, SoundEffects.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.DIAMOND })),
  TURTLE("turtle", 25, new int[] { 2, 5, 6, 2 }, 9, SoundEffects.ITEM_ARMOR_EQUIP_TURTLE, 0.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.SCUTE })),
  SCANDIUM("scandium", 50, new int[] { 3, 6, 8, 3 }, 10, SoundEffects.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.SCANDIUM })),
  PYRITE("pyrite", 45, new int[] { 3, 6, 8, 3 }, 10, SoundEffects.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.PYRITE })),
  LAZURITE("lazurite", 40, new int[] { 3, 6, 8, 3 }, 10, SoundEffects.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F, () -> RecipeItemStack.a(new IMaterial[] { Items.LAZURITE }));
  private static final int[] g;
  
  private final String h;
  
  private final int i;
  
  private final int[] j;
  
  private final int k;
  
  private final SoundEffect l;
  
  private final float m;
  
  private final LazyInitVar<RecipeItemStack> n;
  
  static {
    g = new int[] { 13, 15, 16, 11 };
  }
  
  EnumArmorMaterial(String var2, int var3, int[] var4, int var5, SoundEffect var6, float var7, Supplier<RecipeItemStack> var8) {
    this.h = var2;
    this.i = var3;
    this.j = var4;
    this.k = var5;
    this.l = var6;
    this.m = var7;
    this.n = new LazyInitVar<>(var8);
  }
  
  public int a(EnumItemSlot var0) {
    return g[var0.b()] * this.i;
  }
  
  public int b(EnumItemSlot var0) {
    return this.j[var0.b()];
  }
  
  public int a() {
    return this.k;
  }
  
  public SoundEffect b() {
    return this.l;
  }
  
  public RecipeItemStack c() {
    return this.n.a();
  }
  
  public float e() {
    return this.m;
  }
}
