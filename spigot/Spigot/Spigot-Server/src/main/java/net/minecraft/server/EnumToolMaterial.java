package net.minecraft.server;

import java.util.function.Supplier;

public enum EnumToolMaterial implements ToolMaterial {
  WOOD(0, 59, 2.0F, 0.0F, 15, () -> RecipeItemStack.a(TagsItem.PLANKS)),
  STONE(1, 131, 4.0F, 1.0F, 5, () -> RecipeItemStack.a(new IMaterial[] { Blocks.COBBLESTONE })),
  IRON(2, 250, 6.0F, 2.0F, 14, () -> RecipeItemStack.a(new IMaterial[] { Items.IRON_INGOT })),
  DIAMOND(3, 1561, 8.0F, 3.0F, 10, () -> RecipeItemStack.a(new IMaterial[] { Items.DIAMOND })),
  GOLD(0, 32, 12.0F, 0.0F, 22, () -> RecipeItemStack.a(new IMaterial[] { Items.GOLD_INGOT })),
  SCANDIUM(3, 2500, 9.0F, 3.0F, 10, () -> RecipeItemStack.a(new IMaterial[] { Items.SCANDIUM })),
  PYRITE(3, 2000, 8.0F, 3.0F, 10, () -> RecipeItemStack.a(new IMaterial[] { Items.PYRITE })),
  LAZURITE(3, 1600, 8.0F, 3.0F, 10, () -> RecipeItemStack.a(new IMaterial[] { Items.LAZURITE }));
  private final int f;
  
  private final int g;
  
  private final float h;
  
  private final float i;
  
  private final int j;
  
  private final LazyInitVar<RecipeItemStack> k;
  
  EnumToolMaterial(int var2, int var3, float var4, float var5, int var6, Supplier<RecipeItemStack> var7) {
    this.f = var2;
    this.g = var3;
    this.h = var4;
    this.i = var5;
    this.j = var6;
    this.k = new LazyInitVar<>(var7);
  }
  
  public int a() {
    return this.g;
  }
  
  public float b() {
    return this.h;
  }
  
  public float c() {
    return this.i;
  }
  
  public int d() {
    return this.f;
  }
  
  public int e() {
    return this.j;
  }
  
  public RecipeItemStack f() {
    return this.k.a();
  }
}
