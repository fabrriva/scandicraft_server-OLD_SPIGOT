package net.minecraft.server;

import org.apache.logging.log4j.LogManager;

import com.google.common.collect.Multimap;

public class ItemSword extends ItemToolMaterial {
  private final float a;
  
  private final float b;
  
  public ItemSword(ToolMaterial var0, int var1, float var2, Item.Info var3) {
    super(var0, var3);
    this.b = var2;
    this.a = var1 + var0.c();
  }
  
  public float f() {
    return this.a;
  }
  
  public boolean a(IBlockData var0, World var1, BlockPosition var2, EntityHuman var3) {
    return !var3.isCreative();
  }
  
  public float getDestroySpeed(ItemStack var0, IBlockData var1) {
    Block var2 = var1.getBlock();
    if (var2 == Blocks.COBWEB)
      return 15.0F; 
    Material var3 = var1.getMaterial();
    if (var3 == Material.PLANT || var3 == Material.REPLACEABLE_PLANT || var3 == Material.CORAL || var1.a(TagsBlock.LEAVES) || var3 == Material.PUMPKIN)
      return 1.5F; 
    return 1.0F;
  }
  //@UZApocalyps : Scandicraft changement var0 en var 01
  public boolean a(ItemStack var0, EntityLiving var1, EntityLiving var2) {
	  LogManager.getLogger().info("debug a1 ");
    var0.damage(1, var2, var01 -> var01.broadcastItemBreak(EnumItemSlot.MAINHAND));
    return true;
  }
  //@UZApocalyps : Scandicraft changement var0 en var 01
  public boolean a(ItemStack var0, World var1, IBlockData var2, BlockPosition var3, EntityLiving var4) {
	  LogManager.getLogger().info("debug a2 ");
    if (var2.f(var1, var3) != 0.0F)
      var0.damage(2, var4, var01 -> var01.broadcastItemBreak(EnumItemSlot.MAINHAND)); 
    return true;
  }
  
  public boolean canDestroySpecialBlock(IBlockData var0) {
    return (var0.getBlock() == Blocks.COBWEB);
  }
  
  public Multimap<String, AttributeModifier> a(EnumItemSlot var0) {
    Multimap<String, AttributeModifier> var1 = super.a(var0);
    if (var0 == EnumItemSlot.MAINHAND) {
      var1.put(GenericAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(g, "Weapon modifier", this.a, AttributeModifier.Operation.ADDITION));
      var1.put(GenericAttributes.ATTACK_SPEED.getName(), new AttributeModifier(h, "Weapon modifier", this.b, AttributeModifier.Operation.ADDITION));
    } 
    return var1;
  }
  public EnumAnimation e_(ItemStack var0) {
	    return EnumAnimation.BLOCK;
	  }
	  
	  public int f_(ItemStack var0) {
	    return 72000;
	  }
	  
	 
  //@UZApocalyps : ScandiCraft Ajout BLock sur le clique droit
  public InteractionResultWrapper<ItemStack> a(World var0, EntityHuman var1, EnumHand var2) {
	  	LogManager.getLogger().info("debug InteractronResult ");
	  	ItemStack var3 = var1.b(var2);
	    var1.c(var2);
	    return InteractionResultWrapper.consume(var3);
	  }
}
