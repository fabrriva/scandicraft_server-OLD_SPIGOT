package net.minecraft.server;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nullable;

public class SpawnScepter extends Item {
  private static final Map<EntityTypes<?>, SpawnScepter> a = Maps.newIdentityHashMap();
  
  private final int b;
  
  private final int c;
  
  private final EntityTypes<?> d;
  
  public SpawnScepter(EntityTypes<?> var0, int var1, int var2, Item.Info var3) {
    super(var3);
    this.d = var0;
    this.b = var1;
    this.c = var2;
    a.put(var0, this);
  }
  
  public EnumInteractionResult a(ItemActionContext var0) {
    BlockPosition var7;
    World var1 = var0.getWorld();
    if (var1.isClientSide)
      return EnumInteractionResult.SUCCESS; 
    ItemStack var2 = var0.getItemStack();
    BlockPosition var3 = var0.getClickPosition();
    EnumDirection var4 = var0.getClickedFace();
    IBlockData var5 = var1.getType(var3);
    Block var6 = var5.getBlock();
    if (var6 == Blocks.SPAWNER) {
      TileEntity tileEntity = var1.getTileEntity(var3);
      if (tileEntity instanceof TileEntityMobSpawner) {
        MobSpawnerAbstract mobSpawnerAbstract = ((TileEntityMobSpawner)tileEntity).getSpawner();
        EntityTypes<?> var9 = b(var2.getTag());
        mobSpawnerAbstract.setMobName(var9);
        tileEntity.update();
        var1.notify(var3, var5, var5, 3);
        var2.subtract(1);
        return EnumInteractionResult.SUCCESS;
      } 
    } 
    if (var5.getCollisionShape(var1, var3).isEmpty()) {
      var7 = var3;
    } else {
      var7 = var3.shift(var4);
    } 
    EntityTypes<?> var8 = b(var2.getTag());
    if (var8.spawnCreature(var1, var2, var0.getEntity(), var7, EnumMobSpawn.SPAWN_EGG, true, (!Objects.equals(var3, var7) && var4 == EnumDirection.UP)) != null)
      var2.subtract(1); 
    return EnumInteractionResult.SUCCESS;
  }
  
  public InteractionResultWrapper<ItemStack> a(World var0, EntityHuman var1, EnumHand var2) {
    ItemStack var3 = var1.b(var2);
    MovingObjectPosition var4 = a(var0, var1, RayTrace.FluidCollisionOption.SOURCE_ONLY);
    if (var4.getType() != MovingObjectPosition.EnumMovingObjectType.BLOCK)
      return InteractionResultWrapper.pass(var3); 
    if (var0.isClientSide)
      return InteractionResultWrapper.success(var3); 
    MovingObjectPositionBlock var5 = (MovingObjectPositionBlock)var4;
    BlockPosition var6 = var5.getBlockPosition();
    if (!(var0.getType(var6).getBlock() instanceof BlockFluids))
      return InteractionResultWrapper.pass(var3); 
    if (!var0.a(var1, var6) || !var1.a(var6, var5.getDirection(), var3))
      return InteractionResultWrapper.fail(var3); 
    EntityTypes<?> var7 = b(var3.getTag());
    if (var7.spawnCreature(var0, var3, var1, var6, EnumMobSpawn.SPAWN_EGG, false, false) == null)
      return InteractionResultWrapper.pass(var3); 
    if (!var1.abilities.canInstantlyBuild)
      var3.subtract(1); 
    var1.b(StatisticList.ITEM_USED.b(this));
    return InteractionResultWrapper.success(var3);
  }
  
  public boolean a(@Nullable NBTTagCompound var0, EntityTypes<?> var1) {
    return Objects.equals(b(var0), var1);
  }
  
  public static Iterable<SpawnScepter> f() {
    return Iterables.unmodifiableIterable(a.values());
  }
  
  public EntityTypes<?> b(@Nullable NBTTagCompound var0) {
    if (var0 != null && 
      var0.hasKeyOfType("EntityTag", 10)) {
      NBTTagCompound var1 = var0.getCompound("EntityTag");
      if (var1.hasKeyOfType("id", 8))
        return EntityTypes.a(var1.getString("id")).orElse(this.d); 
    } 
    return this.d;
  }
}
